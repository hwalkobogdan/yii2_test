<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Object_;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $original_id
 * @property int|null $original_parent_id
 */
class Category extends \yii\db\ActiveRecord
{

    const DEFAULT_CATEGORY_ID = 2463;

    private static $categories;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['original_id', 'original_parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'original_id' => 'Original ID',
            'original_parent_id' => 'Original Parent ID',
        ];
    }


    /**
     * @throws Exception
     */
    public static function loadFromXlm($xml)
    {
        if (empty($xml->shop->categories->category)) {
            return false;
        }

        Category::deleteAll();

        foreach ($xml->shop->categories->category as $category) {
            $model = new static();
            $model->name = (string) $category;
            $model->original_id = $category['id'] ?: NULL;
            $model->original_parent_id = $category['parentId'] ?: NULL;
            $model->validate();

            if (! $model->save()) {
                throw new Exception('Ошибка при сохранении категории.');
            }
        }

        return true;
    }


    public static function getCategories()
    {
        if (empty(static::$categories)) {
            static::$categories = array_column(static::find()->all(), NULL, 'original_id');
        }

        return static::$categories;
    }


    public static function getChildId($id)
    {
        $category = Category::getCategories();

        $categoriesId = [$id];
        while (! empty($category[$id]['original_parent_id'])) {
            $id = $category[$id]['original_parent_id'];
            $categoriesId[] = $id;
        }

        return $categoriesId;
    }


    public function getCatToItem()
    {
        return $this->hasOne(CatToItem::class, ['category_id' => 'original_id']);
    }

}
