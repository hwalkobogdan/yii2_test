<?php

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property string $title
 * @property float $price
 * @property float|null $old_price
 * @property string|null $description
 * @property string|null $images
 * @property string|null $other_data
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price', 'old_price'], 'number'],
            [['description', 'other_data'], 'string'],
            [['title', 'images'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'description' => 'Description',
            'images' => 'Images',
            'other_data' => 'Other Data',
        ];
    }


    /**
     * @throws Exception
     */
    public static function extractOfferAndRelated($xml)
    {

        if (empty($xml->shop->offers)) {
            return false;
        }

        Items::deleteAll();
        CatToItem::deleteAll();

        foreach ($xml->shop->offers->offer as $offer) {
            $model = new static();
            $model->title = (string) $offer->name;
            $model->price = (string) $offer->price;
            $model->old_price = 0;
            $model->description = (string) $offer->description;
            $model->images = (string) $offer->picture;
            $model->other_data = json_encode(static::extractParamsFromOffer($offer));
            if (! $model->save()) {
                throw new Exception('Ошибка при сохранении товара.');
            }

            $rel = new CatToItem();
            $rel->item_id = $model->id;
            $rel->category_id = (string) $offer->categoryId;
            if (! $rel->save()) {
                throw new Exception('Ошибка при сохранении связывании категории и товара.');
            }
        }

        return true;
    }


    public static function extractParamsFromOffer($offer)
    {
        $params = [];
        foreach ($offer->param as $param) {
            $params[(string) $param['name']] = (string) $param;
        }

        return $params;
    }


    public function getCategory()
    {
        return $this->hasOne(Category::class, ['original_id' => 'category_id'])
            ->viaTable(CatToItem::tableName(), ['item_id' => 'id']);
    }
}
