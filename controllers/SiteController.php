<?php

namespace app\controllers;

use app\models\Category;
use app\models\Items;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;


class SiteController extends Controller
{

    public function actionIndex()
    {
        $id = Yii::$app->request->get('Category')['original_id'] ?: Category::DEFAULT_CATEGORY_ID;

        $query = Items::find()
            ->joinWith('category')
            ->where(['cat_to_item.category_id' => Category::getChildId($id)]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 3,
        ]);
        $items = $query->offset($pages->offset)->limit($pages->limit)->all();

        $categories = Category::find()
            ->innerJoinWith('catToItem', FALSE)
            ->all();

        return $this->render('index', compact('categories', 'items', 'id', 'pages'));
    }


    public function actionParse()
    {
        if (Yii::$app->request->get('file')) {
            $file = Yii::getAlias('@xml/' . Yii::$app->request->get('file'));
            if (file_exists($file)) {
                $xmlObject = simplexml_load_file($file);

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    Category::loadFromXlm($xmlObject);
                    Items::extractOfferAndRelated($xmlObject);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Данные сохранены!');
                }
                catch (\Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Ошибка сохранения данных!');
                }
            }
        }

        return $this->render('parse');
    }
}
