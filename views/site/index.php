<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
use app\models\Category;

$this->title = 'Товары';

$model = new Category();
$model->original_id = $id;
?>

<div class="site-index">
    <div class="row">
        <div class="col-md-4">
            <?php $form = ActiveForm::begin([
                'id' => 'category-form',
                'method' => 'get'
            ]); ?>

            <?php echo $form->field($model, 'original_id')
                ->dropDownList(array_column($categories, 'name', 'original_id'))
                ->label('Категория');
            ?>

            <div class="form-group">
                <?= Html::submitButton('Показать', ['class' => 'btn btn-primary btn-block', 'name' => 'category-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-8" style="border-left: 1px solid black; ">
            <div class="row">
                <?php if (! empty($items)): ?>
                    <?php foreach ($items as $item): ?>
                        <div class="col-md-4">
                            <div class="item">
                                <div class="img">
                                    <img src="<?=$item->images; ?>" alt="Изображение" width="100%" class="img-thumbnail">
                                </div>
                                <h5>
                                    <?=$item->title; ?>
                                </h5>
                                <p><?= mb_strimwidth($item->description, 0, 100, '...'); ?></p>

                                <span class="badge badge-warning"><?=$item->category->name; ?></span>
                                <span class="badge badge-success"><?=$item->price; ?> ₴</span>

                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="col-md-12">
                        <div id="pagelink">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                            ]); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
