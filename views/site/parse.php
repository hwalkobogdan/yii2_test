<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Парсинг XML';
?>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="<?=Url::to(['site/parse', 'file' => 'agent.xml']); ?>">Agent.xml</a>
        </div>
        <div class="col-md-2">
            <a href="<?=Url::to(['site/parse', 'file' => 'asos.xml']); ?>">Asos.xml</a>
        </div>
    </div>
</div>
