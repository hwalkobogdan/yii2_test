<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cat_to_item}}`.
 */
class m211011_144224_create_cat_to_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cat_to_item}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(11)->unsigned()->notNull(),
            'category_id' => $this->integer(11)->unsigned()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cat_to_item}}');
    }
}
