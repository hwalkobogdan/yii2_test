<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items}}`.
 */
class m211011_144140_create_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%items}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'price' => $this->float()->notNull()->unsigned(),
            'old_price' => $this->float()->unsigned(),
            'description' => $this->text(),
            'images' => $this->string(),
            'other_data' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items}}');
    }
}
